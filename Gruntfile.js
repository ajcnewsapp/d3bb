module.exports = function(grunt) {
  // Project configuration
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    meta: {
      version: "<%= pkg.version %>"
    },

    jshint: {
      source: [
        'src/*.js'
      ],
      build: [
        "build/d3bb.js"
      ],
      options: {
        browser: true,
        curly: true,
        eqeqeq: true,
        latedef: true,
        undef: true,
        unused: true,
        // strict: true,
        trailing: true,
        smarttabs: true,
        indent: 2,
        globals: {
          jQuery: true,
          $: true,
          _: true,
          d3: true,
          Backbone: true
        }
      }
    },

    uglify: {
      options: {
        mangle: { except: ['d3', '_','$', 'Backbone'] }
      },
      build: {
        src: ['src/d3bb.js','src/chart_base.js','src/chart_bar.js','src/chart_line.js','src/chart_pie.js','src/chart_scatterplot.js','src/chart_vbar.js'],
        dest: 'build/<%= pkg.name %>.min.js'
      }
    },

    concat: {
      build: {
        src: ['src/d3bb.js','src/chart_base.js','src/chart_bar.js','src/chart_line.js','src/chart_pie.js','src/chart_scatterplot.js','src/chart_vbar.js'],
        dest: 'build/<%= pkg.name %>.js'
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  // grunt.loadNpmTasks('grunt-contrib-copy');
  // grunt.loadNpmTasks('grunt-contrib-htmlmin');
  // grunt.loadNpmTasks('grunt-contrib-cssmin');
  // grunt.loadNpmTasks('grunt-contrib-watch');
  // grunt.loadNpmTasks('grunt-s3');

  grunt.registerTask('default', ['uglify','concat']);
  grunt.registerTask('lint', ['jshint']);
};

