var d3bb = (function(app, $, _, Backbone, d3) {
  app.ChartVbarView = app.ChartBaseView.extend({
    default: _.defaults({
      barPadding: 0.1
    }, app.ChartBaseView.prototype.defaults),

    initialize: function(options) {
      this.config = _.extend( {}, this.defaults, options );
    },

    getXScale: function() {
      return d3.scale.linear()
        .rangeRound([0, this.width])
        .domain([0, d3.max(this.collection.pluck(this.config.xAttr))]);
    },

    getYScale: function() {
      var padding = this.config.barPadding;
      return d3.scale.ordinal()
        .rangeRoundBands([0, this.height], padding)
        .domain(this.collection.pluck(this.config.yAttr));
    },

    renderAxes: function() {
      this.xAxis = d3.svg.axis()
        .scale(this.scales.x)
        .orient("bottom");

      this.yAxis = d3.svg.axis()
        .scale(this.scales.y)
        .orient("left");

      this.svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + this.height + ")")
        .call(this.xAxis);

      this.svg.append("g")
        .attr("class", "y axis")
        .call(this.yAxis);
    },

    updateChart: function() {
      var chart = this,
          xScale = this.scales.x,
          yScale = this.scales.y,
          data = this.collection.toJSON();

      xScale.domain([0, d3.max(chart.collection.pluck(chart.config.xAttr))]).nice();
      yScale.domain(chart.collection.pluck(chart.config.yAttr));

      var bars = this.svg.selectAll(".bar").data(data);
      bars.exit().remove();
      bars.enter().append("rect")
        .attr("class", "bar")
        .attr("id",     function(d) { return "bar-" + d[chart.config.xAttr]; })
        .attr("data-x", function(d) { return d[chart.config.xAttr]; })
        .attr("data-y", function(d) { return d[chart.config.yAttr]; });

      chart.svg.select(".x.axis").transition().duration(1000).call(chart.xAxis);
      chart.svg.select(".y.axis").transition().duration(1000).call(chart.yAxis);
      bars.transition().duration(1000)
        .attr("x", 0)
        .attr("y", function(d) { return yScale(d[chart.config.yAttr]); })
        .attr("height", yScale.rangeBand())
        .attr("width", function(d) { return xScale(d[chart.config.xAttr]); });
    },

    renderChart: function() {
      var chart = this,
          xScale = this.scales.x,
          yScale = this.scales.y;

      this.svg.selectAll(".bar")
          .data(this.collection.toJSON())
        .enter().append("rect")
          .attr("class", "bar");
          // .attr("id", function(d) { return "bar-" + d[chart.config.xAttr]; })
          // .attr("data-x", function(d) { return d[chart.config.xAttr]; })
          // .attr("data-y", function(d) { return d[chart.config.yAttr]; })
          // .attr("x", 0)
          // .attr("y", function(d) { return yScale(d[chart.config.yAttr]); })
          // .attr("height", yScale.rangeBand())
          // .attr("width", function(d) { return xScale(d[chart.config.xAttr]); });
      this.updateChart()
    }
  });

  return app;
}(d3bb, jQuery, _, Backbone, d3));
