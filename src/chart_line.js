var d3bb = (function(app, $, _, Backbone, d3) {
  app.ChartLineView = app.ChartBaseView.extend({
    defaults: _.defaults({
      yAttr: ["y"],
      addPoints: true,
      pointR: 4,
      colors: d3.scale.category10()
    }, app.ChartBaseView.prototype.defaults),

    initialize: function(options) {
      this.config = _.extend( {}, this.defaults, options );

      // make sure the y-axis variables are in an array, even
      // if there is only one
      if (! Array.isArray(this.config.yAttr)) {
        this.config.yAttr = [this.config.yAttr];
      }
    },

    getXScale: function() {
      var chart = this;
      return d3.scale.linear()
        .range([0, this.width])
        .domain(d3.extent(chart.collection.toJSON(), function(d) { return d[chart.config.xAttr]; }))
        .nice();
    },

    getYScale: function() {
      var chart = this,
          data = this.config.collection.toJSON(),
          yNames = this.config.yAttr,
          yMax = 0;

      _.each(yNames, function(yName) {
        var max = _.max(data, function(d) { return d[yName]; })[yName];
        yMax = yMax < max ? max : yMax;
      });

      return d3.scale.linear()
        .range([chart.height, 0])
        .domain([0,yMax])
        .nice();
    },

    renderAxes: function() {
      this.xAxis = d3.svg.axis()
        .scale(this.scales.x)
        .orient("bottom");

      this.yAxis = d3.svg.axis()
        .scale(this.scales.y)
        .orient("left");

      this.svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + this.height + ")")
        .call(this.xAxis);

      this.svg.append("g")
        .attr("class", "y axis")
        .call(this.yAxis);
    },

    renderChart: function() {
      var chart = this,
          xScale = this.scales.x,
          yScale = this.scales.y,
          x = this.config.xAttr,
          yNames = this.config.yAttr;

      var data = this.collection.toJSON();

      ///
      chart.lines = {};
      _.each(yNames, function(y, i) {
        chart.lines[y] = d3.svg.line()
          .x(function(d) { return xScale(d[x]); })
          .y(function(d) { return yScale(d[y]); });

        chart.svg.append("svg:path")
          .attr("class", "line line-" + y)
          .attr("id", "line-" + y)
          .attr("stroke", chart.config.colors(i))
          .attr("d", chart.lines[y](data));

        if (chart.config.addPoints) {
          chart.svg.selectAll(".point-" + y)
            .data(data)
            .enter().append("circle")
              .attr("class", "point point-" + y)
              .attr("id", function(d,i) { return "line-" + y + "-point-" + i; })
              .attr("data-x", function(d) { return d[x]; })
              .attr("data-y", function(d) { return d[y]; })
              .attr("fill", chart.config.colors(i))
              .attr("cx", function(d) { return xScale(d[x]); })
              .attr("cy", function(d) { return yScale(d[y]); })
              .attr("r", chart.config.pointR);
        }
      });
      ///
    },

    updateChart: function() {
      var chart = this,
          xScale = this.scales.x,
          yScale = this.scales.y,
          x = this.config.xAttr,
          yNames = this.config.yAttr,
          yMax = 0;

      var data = chart.collection.toJSON();

      _.each(yNames, function(yName) {
        var max = _.max(data, function(d) { return d[yName]; })[yName];
        yMax = yMax < max ? max : yMax;
      });

      xScale.domain(d3.extent(data, function(d) { return d[chart.config.xAttr]; })).nice();
      yScale.domain([0,yMax]).nice();

      chart.svg.select(".x.axis").transition().duration(1000).call(chart.xAxis);
      chart.svg.select(".y.axis").transition().duration(1000).call(chart.yAxis);

      ///
      _.each(yNames, function(y) {
        chart.svg.select(".line-" + y).transition().duration(1000)
          .attr("class", "line")
          .attr("d", chart.lines[y](data));

        if (chart.config.addPoints) {

          var points = chart.svg.selectAll(".point-" + y).data(data);
          points.exit().remove();
          points.enter().append("circle")
            .attr("class", "point")
            .attr("id", function(d,i) { return "line-" + y + "-point-" + i; })
            .attr("data-x", function(d) { return d[x]; })
            .attr("data-y", function(d) { return d[y]; });

          points.transition().duration(1000)
            .attr("cx", function(d) { return xScale(d[x]); })
            .attr("cy", function(d) { return yScale(d[y]); })
            .attr("r", chart.config.pointR);
        }
      });
    }
  });
  return app;
}(d3bb, jQuery, _, Backbone, d3));

