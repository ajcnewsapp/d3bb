var d3bb = (function(app, $, _, Backbone, d3) {
  app.ChartScatterplotView = app.ChartBaseView.extend({
    defaults: _.defaults({
      r: 3
    }, app.ChartBaseView.prototype.defaults),

    initialize: function(options) {
      this.config = _.extend( {}, this.defaults, options );
    },

    getXScale: function() {
      return d3.scale.linear()
        .rangeRound([0, this.width])
        .domain([0, d3.max(this.collection.pluck(this.config.xAttr))])
        .nice();
    },

    getYScale: function() {
      return d3.scale.linear()
        .rangeRound([this.height, 0])
        .domain([0, d3.max(this.collection.pluck(this.config.yAttr))])
        .nice();
    },

    renderAxes: function() {
      this.xAxis = d3.svg.axis()
        .scale(this.scales.x)
        .orient("bottom");

      this.yAxis = d3.svg.axis()
        .scale(this.scales.y)
        .orient("left");

      this.svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + this.height + ")")
        .call(this.xAxis);

      this.svg.append("g")
        .attr("class", "y axis")
        .call(this.yAxis);
    },

    renderChart: function() {
      var chart  = this,
          xScale = this.scales.x,
          yScale = this.scales.y;

      this.svg.selectAll(".point")
        .data(this.collection.toJSON())
        .enter().append("circle")
          .attr("class", "point")
          .attr("data-x", function(d) { return d[chart.config.xAttr]; })
          .attr("data-y", function(d) { return d[chart.config.yAttr]; })
          .attr("cx", function(d) { return xScale(d[chart.config.xAttr]); })
          .attr("cy", function(d) { return yScale(d[chart.config.yAttr]); })
          .attr("r", chart.config.r);
    },

    updateChart: function() {
      var chart  = this,
          xScale = this.scales.x,
          yScale = this.scales.y,
          data   = this.collection.toJSON();

      xScale.domain([0, d3.max(this.collection.pluck(this.config.xAttr))]).nice();
      yScale.domain([0, d3.max(this.collection.pluck(this.config.yAttr))]).nice();

      chart.svg.select(".x.axis").transition().duration(1000).call(chart.xAxis);
      chart.svg.select(".y.axis").transition().duration(1000).call(chart.yAxis);

      var points = this.svg.selectAll(".point").data(data);
      points.exit().remove();
      points.enter().append("circle")
        .attr("class", "point")
        .attr("data-x", function(d) { return d[chart.config.xAttr]; })
        .attr("data-y", function(d) { return d[chart.config.yAttr]; });

      points.transition().duration(1000)
        .attr("cx", function(d) { return xScale(d[chart.config.xAttr]); })
        .attr("cy", function(d) { return yScale(d[chart.config.yAttr]); })
        .attr("r", chart.config.r);
    }
  });
  return app;
}(d3bb, jQuery, _, Backbone, d3));
