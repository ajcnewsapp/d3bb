var d3bb = (function(app, $, _, Backbone, d3) {
  app.ChartPieView = app.ChartBaseView.extend({
    defaults: _.defaults({
      labels: "labels",
      colors: d3.scale.category20(),
      innerRadius: 0
    }, app.ChartBaseView.prototype.defaults),

    initialize: function(options) {
      this.config = _.extend( {}, this.defaults, options );
    },

    renderChart: function() {
      var chart = this,
          color = this.config.colors,
          outerRadius = Math.min(this.height, this.width) / 2,
          innerRadius = this.config.innerRadius;

      chart.arc = d3.svg.arc()
        .innerRadius(innerRadius)
        .outerRadius(outerRadius);

      chart.pie = d3.layout.pie()
        .value(function(d) { return d[chart.config.yAttr]; })
        .sort(function(d) { return d[chart.config.labels]; });

      var arcs = chart.svg.selectAll("g.arc")
        .data(chart.pie( chart.collection.toJSON() )).enter().append("g")
          .attr("class", "arc")
          .attr("transform", "translate(" + outerRadius + "," + outerRadius + ")");

      arcs.append("path")
        .attr("fill", function(d,i) { return color(i); })
        .attr("d", chart.arc)
        .each(function(d) { this._current = d; });

      arcs.append("text")
        .attr("transform", function(d) { return "translate(" + chart.arc.centroid(d) + ")"; })
        .attr("text-anchor", "middle")
        .text(function(d) { return d.value; })
          .attr("class", "data-label");
    },

    updateChart: function() {
      var chart = this,
          color = this.config.colors,
          outerRadius = this.height / 2;

      var arcs = chart.svg.selectAll("g.arc").data(chart.pie( chart.collection.toJSON() ));

      arcs.exit().remove();
      arcs.enter().append("g")
          .attr("class", "arc")
          .attr("transform", "translate(" + outerRadius + "," + outerRadius + ")");
      arcs.append("path")
        .transition().duration(1000)
        .attr("fill", function(d,i) { return color(i); })
        .attr("d", chart.arc);

      arcs.append("text")
        .attr("transform", function(d) { return "translate(" + chart.arc.centroid(d) + ")"; })
        .attr("text-anchor", "middle")
        .text(function(d) { return d.value; })
          .attr("class", "data-label");
    }
  });
  return app;
}(d3bb, jQuery, _, Backbone, d3));
