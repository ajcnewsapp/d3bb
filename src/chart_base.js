var d3bb = (function(app, $, _, Backbone, d3) {
  app.ChartBaseView = Backbone.View.extend({
    defaults: {
      margin: {left: 50, right: 70, bottom: 30, top: 20},
      xAttr: 'x',
      yAttr: 'y',
      chartContainer: "#chart",
      templateText: {}
    },

    constructor: function(options) {
      Backbone.View.prototype.constructor.apply(this, arguments);
      this.listenTo(this, 'show', this.afterRender);
      this.listenTo(this.collection, 'add', this.updateChart);
      this.listenTo(this.collection, 'remove', this.updateChart);
      this.listenTo(this.collection, 'reset', this.updateChart);
    },

    initialize: function(options) {
      this.config = _.extend( {}, this.defaults, options );
    },

    events : {
      "load": "appendChart"
    },

    serializeData: function() {},
    getXScale:  function() {},
    getYScale:  function() {},
    renderAxes: function() {},
    renderData: function() {},
    updateChart: function () {},

    getScales : function() {
      this.scales = {
        x: this.getXScale(),
        y: this.getYScale()
      };
    },

    render: function() {
      var template = _.template( $(this.config.template).html() );
      this.$el.append(template( this.config.templateText ));

      if (this.onRender) {
        this.onRender();
      }

      this.trigger("show");
      return this;
    },

    afterRender: function() {
      var margin = this.config.margin;

      this.width = this.$el.width() - margin.left - margin.right;
      this.height = this.$el.height() - margin.top - margin.bottom;

      this.svg = d3.select(this.config.chartContainer).append("svg")
          .attr("width", this.width + margin.left + margin.right)
          .attr("height", this.height + margin.top + margin.bottom)
        .append("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      this.getScales();
      this.renderAxes();
      this.renderChart();
    }
  });
  return app;
}(d3bb, jQuery, _, Backbone, d3));
